# dotfiles ~/.

![minguyen's dotfiles](https://gitlab.com/minguyen/dotfiles/-/raw/7331c1515c060ff1b5b2c1c88b61f3c6f257d35c/screenshot.png)

## Configuration files for:
 
- zsh (shell)
    - Ctrl + left/Ctrl + right are remapped to move the cursor.
    - .zshrc is located in ~/.config/zsh/
    - Aliases are stored in ~/.config/aliasrc
    - Environmental variables are stored in ~/.config/zsh/zprofile
    - xinitrc is located in ~/.config/x11/
- bash (alternative shell)
- alacritty (GPU-accelerated terminal emulator)
- i3-gaps (tiling window manager)
- i3-status (status bar)
- neovim (text editor) with following plugins:
    - [Vundle](https://github.com/VundleVim/Vundle.vim) - A plug-in manager for Vim.
    - [Lightline](https://github.com/itchyny/lightline.vim) - A light and configurable statusline for Vim.
    - [Vim Instant Markdown](https://github.com/suan/vim-instant-markdown) - Instant Markdown for Vim.
    - [Vim Rainbow](https://github.com/nfischer/vim-rainbows) - Vim runtime file.s
    - [Vifm](https://github.com/vifm/vifm) - File manager with curses interface.
    - [Nerdtree](https://github.com/preservim/nerdtree) - A tree explorer for Vim.
        - [Nerdtree Syntax Highlight](https://github.com/tiagofumo/vim-nerdtree-syntax-highlight) - Adds coloring to Nerdtree
    - [Vim Devicons](https://github.com/ryanoasis/vim-devicons) - File type icons for Vim.
    - [nnn](https://github.com/mcchrish/nnn.vim) - File manager for Vim.
    - [Vimwiki](https://github.com/vimwiki/vimwiki) - Personal Wiki for Vim.
    - [Vimagit](https://github.com/jreybert/vimagit) - Easier Git workflow in Vim.
    - [NeoTex](https://github.com/donRaphaco/neotex) - A Vim Plugin for Lively Previewing LaTeX PDF Output. (Archived)
    - [Surround](https://github.com/tpope/vim-surround) - A plugin for quoting and parenthesizing.
    - [i3 Vim Syntax](https://github.com/PotatoesMaster/i3-vim-syntax) - i3 Syntax Highlighter for Vim.
    - [Sxhkd Vim](https://github.com/kovetskiy/sxhkd-vim) - Vim plugin for sxhkd.
    - [Python Syntax](https://github.com/vim-python/python-syntax) - Python syntax highlighting.
    - [CSS Color](https://github.com/ap/vim-css-color) - A very fast, multi-syntax context-sensitive color name highlighter.
    - [Nord](https://github.com/arcticicestudio/nord-vim) - Nord Theme for Vim.
    - [Goyo](https://github.com/junegunn/goyo.vim) - Distraction free writing in Vim.
    - [Limelight](https://github.com/junegunn/limelight.vim) - Hyperfocus writing in Vim.
    - [Vim Emoji](https://github.com/junegunn/vim-emoji) - Emojis.
- brave (browser)
- bspwm (tiling window manager)
- calcurse (terminal based calendar)
- dunst (notfication daemon)
- gtk theme (by [Eliver L.](https://github.com/EliverLara/Nordic))
- mpd (server-side application for music)
- mpv (media player)
- newsboat (RSS/Atom reader)
- sxhkd (simple X Hotkey Daemon)
- sxiv (simple X Image Viewer)
- zathura (document viewer)
## Also included:
- Executable scripts located in .local/bin to
    - toggle airplane mode
    - toggle webcam
    - record screen/webcam/audio
    - (un)mount external devices in dmenu
    - select emojis in dmenu
    - extract archived files easily
    - take screenshots in dmenu
    - enable a status bar in dwm
    - execute non-steam programs with proton (Proton required!)
    - get a [cheatsheet](https://github.com/cheat/cheatsheets) when typing `cheat` and the desired cheatsheet. 
    - get random man pages
    - compile files
